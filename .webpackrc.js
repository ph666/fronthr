const path = require('path');
export default {
  extraBabelPlugins: [
    ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }]
  ],
  proxy: {
    "/api": {
      "target": "http://localhost:8083/",
      "changeOrigin": true,
      "pathRewrite": { "^/api" : "" }
    }
  },
  env: {
    development: {
      extraBabelPlugins: [
        "dva-hmr"
      ]
    }
  },
  alias: {
    "@": path.resolve(__dirname, 'src')
  },
  disableCSSModules: true // 很重要让less样式生效
}
