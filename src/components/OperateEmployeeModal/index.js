import React from 'react';
import { Modal, Form, Input, Radio, DatePicker, Select } from 'antd';
import { addNewEmp } from '@/services/employee';
const RadioGroup = Radio.Group;
const { Option } = Select;
/**
 * type 代表表单元素的类型
 * 1 input
 * 2 select
 * 3 datePicker
 * 4 radio
 */
const FormItemList = [
  {
    type: 1,
    name: 'name',
    placeholder: '请输入员工姓名',
    label: '员工姓名',
    rules: [
      { required: true, message: '请输入员工姓名' }
    ]
  },
  {
    type: 4,
    name: 'gender',
    placeholder: '请选择性别',
    label: '性别',
    rules: [
      { required: true, message: '请选择性别' }
    ],
    radios: [
      {
        id: '男'
      },
      {
        id: '女'
      },
    ]
  },
  {
    type: 3,
    name: 'birthday',
    placeholder: '请选择出生日期',
    label: '出生日期',
    rules: [
      { required: true, message: '请选择出生日期' }
    ]
  },
  {
    type: 2,
    name: 'politicId',
    placeholder: '请选择政治面貌',
    label: '政治面貌',
    rules: [
      { required: true, message: '请选择政治面貌' }
    ],
    options: []
  },
  {
    type: 2,
    name: 'nationId',
    placeholder: '请选择民族',
    label: '民族',
    rules: [
      { required: true, message: '请选择民族' }
    ],
    options: []
  },
  {
    type: 1,
    name: 'nativePlace',
    placeholder: '请输入籍贯',
    label: '籍贯',
    rules: [
      { required: true, message: '请输入籍贯' }
    ]
  },
  {
    type: 1,
    name: 'email',
    placeholder: '请输入电子邮箱',
    label: '电子邮箱',
    rules: [
      { required: true, message: '请输入电子邮箱' }
    ]
  },
  {
    type: 1,
    name: 'address',
    placeholder: '请输入联系地址',
    label: '联系地址',
    rules: [
      { required: true, message: '请输入联系地址' }
    ]
  },
  {
    type: 2,
    name: 'posId',
    placeholder: '请选择职位',
    label: '职位',
    rules: [
      { required: true, message: '请选择职位' }
    ],
    options: []
  },
  {
    type: 2,
    name: 'jobLevelId',
    placeholder: '请选择职称',
    label: '职称',
    rules: [
      { required: true, message: '请选择职称' }
    ],
    options: []
  },
  {
    type: 2,
    name: 'departmentId',
    placeholder: '请选择所属部门',
    label: '所属部门',
    rules: [
      { required: true, message: '请选择所属部门' }
    ],
    options: []
  },
  {
    type: 1,
    name: 'phone',
    placeholder: '请输入电话号码',
    label: '电话号码',
    rules: [
      { required: true, message: '请输入电话号码' }
    ]
  },
  {
    type: 1,
    name: 'workID',
    placeholder: '请输入工号',
    label: '工号',
    rules: [
      { required: true, message: '请输入工号' }
    ],
    disabled: true
  },
  {
    type: 2,
    name: 'tiptopDegree',
    placeholder: '请选择最高学历',
    label: '学历',
    rules: [
      { required: true, message: '请选择最高学历' }
    ],
    options: [
      { id: '大专' },
      { id: '本科' },
      { id: '硕士' },
      { id: '博士' },
      { id: '高中' },
      { id: '初中' },
      { id: '小学' },
      { id: '其他' }
    ]
  },
  {
    type: 1,
    name: 'school',
    placeholder: '请输入毕业院校',
    label: '毕业院校',
    rules: [
      { required: true, message: '请输入毕业院校' }
    ]
  },
  {
    type: 1,
    name: 'specialty',
    placeholder: '请输入专业名称',
    label: '专业名称',
    rules: [
      { required: true, message: '请输入专业名称' }
    ]
  },
  {
    type: 3,
    name: 'beginDate',
    placeholder: '请选择入职日期',
    label: '入职日期',
    rules: [
      { required: true, message: '请选择入职日期' }
    ]
  },
  {
    type: 3,
    name: 'conversionTime',
    placeholder: '请选择转正日期',
    label: '转正日期',
    rules: [
      { required: true, message: '请选择转正日期' }
    ]
  },
  {
    type: 3,
    name: 'beginContract',
    placeholder: '请选择合同起始日期',
    label: '合同起始日期',
    rules: [
      { required: true, message: '请选择合同起始日期' }
    ]
  },
  {
    type: 3,
    name: 'endContract',
    placeholder: '请选择合同终止日期',
    label: '合同终止日期',
    rules: [
      { required: true, message: '请选择合同终止日期' }
    ]
  },
  {
    type: 1,
    name: 'idCard',
    placeholder: '请输入身份证号码',
    label: '身份证号码',
    rules: [
      { required: true, message: '请输入身份证号码' }
    ]
  },
  {
    type: 4,
    name: 'engageForm',
    placeholder: '请选择聘用形式',
    label: '聘用形式',
    rules: [
      { required: true, message: '请选择聘用形式' }
    ],
    radios: [
      {
        id: '劳动合同'
      },
      {
        id: '劳务合同'
      },
    ]
  },
  {
    type: 4,
    name: 'wedlock',
    placeholder: '请选择婚姻状况',
    label: '婚姻状况',
    rules: [
      { required: true, message: '请选择婚姻状况' }
    ],
    radios: [
      {
        id: '已婚'
      },
      {
        id: '未婚'
      },
      {
        id: '离异'
      },
    ]
  },
]
const NameMapToOptionKey = { // 根据select表单字段映射后台返回的option列表字段
  politicId: 'politics',
  nationId: 'nations',
  posId: 'positions',
  jobLevelId: 'joblevels',
  departmentId: 'deps'
}
const DateKeys = [ 'birthday', 'beginDate', 'conversionTime', 'beginContract', 'endContract']

class OperateEmployeeModal extends React.PureComponent {

  state = {
    options: {
      politics: [], // 政治面貌
      nations: [], // 民族
      positions: [], // 职位
      joblevels: [], // 职称
      deps: [], // 所属部门
      workID: '' // 工号
    }
  }

  componentDidMount() {
    // console.log('didMountProps', this.props)
    // console.log('didMount', this.props.empBasicData)
  }

  componentWillReceiveProps(nextProps) {
    const { empBasicData, form } = this.props
    const { setFieldsValue } = form
    if (nextProps.empModalVisible && !this.props.empModalVisible) {
      setFieldsValue({
        workID: empBasicData.workID
      })
    }
  }

  handleOk = () => {
    this.props.form.validateFields((err, values) => {
      const formatDateValues = {}
      if (!err) {
        Object.keys(values).forEach((key) => {
          if (DateKeys.includes(key)) {
            formatDateValues[key] = values[key].format('YYYY-MM-DD')
          } else {
            formatDateValues[key] = values[key]
          }
        })
        console.log('formatDateValues', formatDateValues)
        addNewEmp(formatDateValues).then(res => {
          if (res && res.code === '0') {
            this.props.handleEmpModal(false, 'refresh')
          }
        })
      }
    })
  }

  handleCancel = () => {
    this.props.handleEmpModal(false)
  }

  filterItem = (item) => {
    const { getFieldDecorator } = this.props.form
    const { empBasicData } = this.props
    const optionKey = NameMapToOptionKey[item.name]
    let options = item.options // 前端写死的options
    if (optionKey) { // 后台拿数据
      options = empBasicData[optionKey]
    }
    let result = null
    switch(item.type) {
      case 1:
        result = getFieldDecorator(item.name, {
          rules: item.rules
        })(
          <Input disabled={item.disabled} placeholder={item.placeholder} />
        )
        break
      case 2:
        result = getFieldDecorator(item.name, {
          rules: item.rules
        })(
          <Select placeholder={item.placeholder} style={{width: '150px'}}>
            {
              options.map(option => (
                <Option value={option.id} key={option.id}>
                  {option.name || option.id}
                </Option>
              ))
            }
          </Select>
        )
        break
        case 3:
          result = getFieldDecorator(item.name, {
            rules: item.rules
          })(
            <DatePicker  placeholder={item.placeholder} />
          )
        break
        case 4:
          result = getFieldDecorator(item.name, {
            rules: item.rules
          })(
            <RadioGroup>
              {
                item.radios.map(radio => (
                  <Radio key={radio.id} value={radio.id}>{radio.name || radio.id}</Radio>
                ))
              }
            </RadioGroup>
          )
        break
      default:
    }
    return result
  }

  showFormItem = (formItems) => {
    const htmlItems = formItems.map(item => {
      return (
        <Form.Item key={item.name} label={item.label}>
          {this.filterItem(item)}
        </Form.Item>
      )
    })
    return htmlItems
  }

  render() {
    const { empModalVisible } = this.props
    return (
      <div className="operate-employee-container">
        <Modal
          title="员工框"
          visible={empModalVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          width={1100}
        >
          <Form layout="inline">
            {
              this.showFormItem(FormItemList)
            }
          </Form>
        </Modal>
      </div>
    )
  }
}
export default Form.create()(OperateEmployeeModal);
