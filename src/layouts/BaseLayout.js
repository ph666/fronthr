import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import './BaseLayout.less';
const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

class BaseLayout extends React.PureComponent {

  state = {
    collapsed: false
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  onSelectMenu = ({key}) => {
    this.props.dispatch(routerRedux.push({
      pathname: key
    }))
  }

  render() {
    const { collapsed } = this.state
    return (
      <Layout className="base-layout-container">
        <Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
        >
          <div className="logo"></div>
          <Menu theme="dark" mode="inline" onSelect={this.onSelectMenu}>
            <SubMenu key="7" title="用户">
              <Menu.Item key="/register">
                <Icon type="user" />
                <span>注册用户</span>
              </Menu.Item>
              <Menu.Item key="/index">
                <Icon type="video-camera" />
                <span>nav 2</span>
              </Menu.Item>
            </SubMenu>
            <Menu.Item key="/employee">
              <Icon type="user" />
              <span>员工</span>
            </Menu.Item>
            <Menu.Item key="2">
              <Icon type="video-camera" />
              <span>nav 2</span>
            </Menu.Item>
            <Menu.Item key="3">
              <Icon type="upload" />
              <span>nav 3</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ backgroundColor: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={ collapsed ? 'menu-unfold' : 'menu-fold' }
              onClick={this.toggle}
            />
          </Header>
          <Content style={{
            margin: '24px 16px', padding: 24, backgrond: '#fff', minHeight: 280
          }}>
            {this.props.children}
          </Content>
        </Layout>
      </Layout>
    )
  }
}

export default connect()(BaseLayout);
