import React from 'react';
const BlankLayout = (props) => (
  <div>
    {props.children}
  </div>
)
export default BlankLayout;
