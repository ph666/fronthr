import { getEmpBasicData } from '@/services/employee';
const initSearchForm = {
  keywords: undefined, // 关键字
  politicId: undefined, // 政治面貌id
  posId: undefined, // 职位id
  jobLevelId: undefined, // 职称id
  departmentId: undefined, // 所属部门id
  engageForm: undefined, // 劳务合同
  beginDateScope: '' // 入职日期范围
}
export default {
  namespace: 'employee',
  state: {
    empBasicData: {
      politics: [], // 政治面貌
      nations: [], // 民族
      positions: [], // 职位
      joblevels: [], // 职称
      deps: [], // 所属部门
      workID: '' // 工号
    },
    searchForm: initSearchForm
  },
  effects: {
    *fetchEmpBasicData(action, { call, put }) {
      const res = yield call(getEmpBasicData)
      if (res && res.code === '0') {
        yield put({
          type: 'saveState',
          payload: { empBasicData: res.data }
        })
      }
    },
    *changeFormItem({ payload }, { put, select }) {
      const oldSearchForm = yield select(({employee}) => (employee.searchForm))
      yield put({
        type: 'saveState',
        payload: {
          searchForm: {
            ...oldSearchForm,
            ...payload
          }
        }
      })
    },
    *resetForm({ payload }, { put }) {
      yield put({
        type: 'saveState',
        payload: {
          searchForm: initSearchForm
        }
      })
    }
  },
  reducers: {
    saveState(state, action) {
      return {
        ...state,
        ...action.payload
      }
    }
  }
}
