import React from 'react';
import { LocaleProvider } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import { Router, Route, Switch, routerRedux } from 'dva/router';
import dynamic from 'dva/dynamic';
import routes from './routes/index';
import BaseLayout from '@/layouts/BaseLayout';
import Login from '@/view/Login';
const { ConnectedRouter } = routerRedux;

function RouterConfig({ history, app }) {
  return (
    <LocaleProvider locale={zh_CN}>
      <ConnectedRouter history={history}>
        <Router history={history}>
          <Switch>
            <Route path="/" exact component={Login} />
            <BaseLayout>
              {
                routes.map(({ path, ...rest}, key) => {
                  return (
                    <Route key={key}
                      exact
                      path={path}
                      component={dynamic({
                        app,
                        ...rest,
                      })}
                    />
                  )
                })
              }
            </BaseLayout>
          </Switch>
        </Router>
      </ConnectedRouter>
    </LocaleProvider>
  );
}

export default RouterConfig;
