const routes = [
  {
    path: '/',
    component: () => import('@/view/Login')
  },
  {
    path: '/index',
    models: () => [import('@/models/example')],
    component: () => import('@/view/IndexPage')
  },
  {
    path: '/welcome',
    component: () => import('@/view/Welcome')
  },
  {
    path: '/register',
    component: () => import('@/view/Register')
  },
  {
    path: '/employee',
    models: () => [import('@/models/employee')],
    component: () => import('@/view/Employee')
  },
]
export default routes;
