import request from '@/utils/request';
/**
 * 获取增加员工的下拉框的数据
 */
export function getEmpBasicData() {
  return request('/employee/basic/basicdata')
}
/**
 * 添加新的员工
 */
export function addNewEmp(params) {
  return request.post('/employee/basic/emp', params, {}, true)
}

/**
 * 获取员工列表
 */
export function getEmpList(body) {
  return request('/employee/basic/emp', {body})
}


/**
 * 导出全部员工excel
 */
export function exportExcel() {
  return request('/employee/basic/exportEmp')
}

/**
 * 导入员工excel
 */
export function importExcel(body) {
  return request.post('/employee/basic/importEmp', body, {}, false, true)
}
