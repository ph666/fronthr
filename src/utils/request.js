import fetch from 'dva/fetch';
import { stringify } from 'qs';

function parseJSON(response) {
  return response.json();
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

request.post = (url, body, config, isFormData, isFile) => {
  return request(url, {
    method: 'POST',
    body,
    isFormData,
    isFile,
    ...config
  })
}

request.put = (url, body, config, isFormData) => {
  return request(url, {
    method: 'PUT',
    body,
    isFormData,
    ...config
  })
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  let newOptions = {
    method: 'GET',
    credentials: 'include'
  }
  Object.assign(newOptions, options)
  if (newOptions.method === 'GET') {
    Object.assign(newOptions, {
      headers: {
        'Content-Type': 'application/json'
      },
    })
    if (newOptions.body) {
      // const bodyKeys =  Object.keys(newOptions.body)
      let searchStr = ''
      if(newOptions.body instanceof Object) {
        // bodyKeys.forEach((key, index) => {
        //   searchStr += key + '=' + newOptions.body[key] + (bodyKeys.length === index + 1 ? '' : '&')
        // })
        searchStr = stringify(newOptions.body)
      }
      url += '?' + searchStr
    }
    delete newOptions.body
  } else if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
    let newBody = null
    if (options.isFormData && !options.isFile) { // 是formData 格式传参
      // const bodyKeys =  Object.keys(options.body)
      let searchStr = ''
      if(options.body instanceof Object) {
        // bodyKeys.forEach((key, index) => {
        //   searchStr += key + '=' + options.body[key] + (bodyKeys.length === index + 1 ? '' : '&')
        // })
        searchStr = stringify(options.body)
      }
      newBody = searchStr
      Object.assign(newOptions, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: newBody
      })
      delete newOptions.isFormData
    } else if (options.isFile) { // 上传文件一定不要设置请求头不然无法上传
      Object.assign(newOptions, {
        // headers: {
        //   'Content-Type': 'multipart/form-data'
        // },
        body: options.body
      })
      delete newOptions.isFile
    } else {
      newBody = JSON.stringify(options.body)
      Object.assign(newOptions, {
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: newBody
      })
    }
  }
  return fetch('/api' + url, newOptions)
    .then(checkStatus)
    .then(parseJSON)
    // .then(data => data)
    .catch(err => err);
}
