import React from 'react';
import { Button, Input, Form, Select, Radio, DatePicker, Table } from 'antd';
import { connect } from 'dva'
import OperateEmployeeModal from '@/components/OperateEmployeeModal';
import { getEmpList, importExcel } from '@/services/employee';
import { dateFormat } from '@/utils/index';
import './index.less';

const { Option } = Select;
const RadioGroup = Radio.Group;
const { RangePicker } = DatePicker;

const Selects = [
  {
    name: 'politicId',
    optionKey: 'politics',
    label: '政治面貌'
  },
  {
    name: 'nationId',
    optionKey: 'nations',
    label: '民族'
  },
  {
    name: 'posId',
    optionKey: 'positions',
    label: '职位'
  },
  {
    name: 'jobLevelId',
    optionKey: 'joblevels',
    label: '职称'
  },
  {
    name: 'departmentId',
    optionKey: 'deps',
    label: '所属部门'
  }
]

@connect(({ employee }) => ({ employee }))
class Employee extends React.PureComponent {

  state = {
    empModalVisible: false,
    emps: [],
    total: 0
  }

  componentDidMount() {
    this.fetchBasicData()
    this.fetchTable()
  }

  fetchTable = () => {
    const { employee: { searchForm } } = this.props;
    getEmpList(searchForm).then(res => {
      this.setState({
        emps: res.data.emps,
        total: res.data.count
      })
    })
  }

  fetchBasicData = () => {
    this.props.dispatch({
      type: 'employee/fetchEmpBasicData'
    })
  }

  openAddModel = () => {
    this.setState({
      empModalVisible: true
    })
  }

  handleEmpModal = (val, refresh) => {
    this.setState({
      empModalVisible: val
    })
    if (refresh === 'refresh') {
      this.fetchBasicData()
    }
  }

  handleFormItem = (name, val) => {
    this.props.dispatch({
      type: 'employee/changeFormItem',
      payload: {
        [name]: val
      }
    })
  }

  resetForm = async () => {
    this.props.form.resetFields()
    await this.props.dispatch({
      type: 'employee/resetForm'
    })
    this.fetchTable()
  }

  exportExcel = () => {
    window.open("/api/employee/basic/exportEmp", "_self");
  }

  importExcel = (e) => {
    const file = e.target.files[0]
    console.log(file)
    const formdata = new FormData()
    formdata.append('file', file)
    importExcel(formdata)
  }

  render() {
    const { employee: { empBasicData, searchForm } } = this.props
    const { empModalVisible, emps } = this.state
    const Columns = [
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '工号',
        dataIndex: 'workID',
      },
      {
        title: '性别',
        dataIndex: 'gender',
      },
      {
        title: '出生日期',
        dataIndex: 'birthday',
        render: text => dateFormat(text)
      },
      {
        title: '身份证号码',
        dataIndex: 'idCard',
      },
      {
        title: '婚姻状况',
        dataIndex: 'wedlock',
      },
      {
        title: '民族',
        dataIndex: 'nation.name',
      },
      {
        title: '籍贯',
        dataIndex: 'nativePlace',
      },
      {
        title: '政治面貌',
        dataIndex: 'politicsStatus.name',
      },
      {
        title: '电子邮件',
        dataIndex: 'email',
      },
      {
        title: '电话号码',
        dataIndex: 'phone',
      },
      {
        title: '联系地址',
        dataIndex: 'address',
      },
      {
        title: '所属部门',
        dataIndex: 'department.name',
      },
      {
        title: '职位',
        dataIndex: 'postion.name',
      },
      {
        title: '职称',
        dataIndex: 'jobLevel.name',
      },
      {
        title: '聘用形式',
        dataIndex: 'engageForm',
      },
      {
        title: '入职日期',
        dataIndex: 'beginDate',
        render: text => dateFormat(text)
      },
      {
        title: '转正日期',
        dataIndex: 'conversionTime',
        render: text => dateFormat(text)
      },
      {
        title: '合同起始日期',
        dataIndex: 'beginContract',
        render: text => dateFormat(text)
      },
      {
        title: '合同截止日期',
        dataIndex: 'endContract',
        render: text => dateFormat(text)
      },
      {
        title: '合同期限',
        dataIndex: 'contractTerm',
      },
      {
        title: '最高学历',
        dataIndex: 'tiptopDegree',
      }
    ]
    return (
      <div className="employee-container">
        <div className="operate-line">
          <Button type="primary" onClick={this.openAddModel}>添加员工</Button>
        </div>
        <Form className="form-qurey-container" layout="inline">
          <Form.Item>
            <Input value={searchForm.keywords} className="form-item-width" placeholder="通过员工名搜索员工" onChange={val => this.handleFormItem('keywords', val)} />
          </Form.Item>
          {
            Selects.map(select => (
              <Form.Item label={select.label} key={select.name}>
                <Select className="form-item-width" value={searchForm[select.name]} placeholder={select.label} onChange={val => this.handleFormItem(select.name, val)}>
                  {
                    empBasicData[select.optionKey].map(item => (
                      <Option key={item.id} value={item.id}>{item.name}</Option>
                    ))
                  }
                </Select>
              </Form.Item>
            ))
          }
          <Form.Item label="聘用形式">
            <RadioGroup value={searchForm.engageForm} onChange={e => this.handleFormItem('engageForm', e.target.value)}>
              <Radio value="劳动合同">劳动合同</Radio>
              <Radio value="劳务合同">劳务合同</Radio>
            </RadioGroup>
          </Form.Item>
          <Form.Item label="入职日期">
            <RangePicker onChange={(date, dateString) => this.handleFormItem('beginDateScope', dateString.join())} />
          </Form.Item>
          <div className="query-line">
           <Button type="primary" onClick={this.fetchTable}>查询</Button>
           <Button onClick={this.resetForm}>重置</Button>
           <Button type="primary" onClick={this.exportExcel}>导出</Button>
           {/* <Button type="primary" onClick={this.importExcel}>导入</Button> */}
           <Input type="file" onChange={this.importExcel} />
          </div>
        </Form>
        <Table
          rowKey="id"
          columns={Columns}
          dataSource={emps}
          scroll={{x: 1700}}
        />
        <OperateEmployeeModal empBasicData={empBasicData} empModalVisible={empModalVisible} handleEmpModal={this.handleEmpModal} />
      </div>
    )
  }
}
export default Form.create()(Employee);
