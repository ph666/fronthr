import React from 'react';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import { Button } from 'antd';
import './IndexPage.css';

class IndexPage extends React.Component {

  componentDidMount() {
    console.log(this.props)
  }

  jumpToPage = () => {
    this.props.dispatch(routerRedux.push({
      pathname: '/'
    }))
  }

  render() {
    return (
      <div className="normal">
        <h1 className="title">Yay! Welcome to dva!</h1>
        <div className="welcome" />
        <ul className="list">
          <li>To get started, edit <code>src/index.js</code> and save to reload.</li>
          <li><a href="https://github.com/dvajs/dva-docs/blob/master/v1/en-us/getting-started.md">Getting Started</a></li>
        </ul>
        <Button onClick={this.jumpToPage}>跳转</Button>
      </div>
    );
  }
}

IndexPage.propTypes = {
};

export default connect()(IndexPage);
