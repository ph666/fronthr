import React from 'react';
import { Input, Button, message } from 'antd';
import request from '@/utils/request';
import './index.less';
class LoginPage extends React.Component {

  state = {
    username: 'admin',
    password: '123'
  }

  componentDidMount() {
    console.log(this.props)
    request('/test', {
      method: 'get'
    })
  }

  changeInput = (type, value) => {
    this.setState({
      [type]: value
    })
  }

  login = () => {
    const { username, password } = this.state;
    if (!username || !password) {
      message.warning('请输入用户名和密码')
      return
    }
    request.post('/login', {username, password}, {}, true).then(res => {
      console.log('res1', res)
      if(res && res.code === '0') {
        console.log('res', res)
        this.props.history.push('/welcome')
      }
    })
  }

  render() {
    const { username, password } = this.state
    return (
      <div className="login-container">
        <div className="login-box">
          <ul>
            <li>
              <Input placeholder="请输入用户名" style={{width: '300px'}} value={username} onChange={e => this.changeInput('username',e.target.value)} />
            </li>
            <li>
              <Input placeholder="请输入密码" style={{width: '300px'}} value={password} onChange={e => this.changeInput('password',e.target.value)} />
            </li>
          </ul>
          <div className="login-btn">
            <Button type="primary" onClick={this.login}>登录</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default LoginPage;
