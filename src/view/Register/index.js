import React from 'react';
import { Input, Button, message } from 'antd';
import request from '@/utils/request';
import './index.less';
class Register extends React.PureComponent {

  state = {
    username: '',
    password: ''
  }

  changeInput = type => e => {
    const { value } = e.target
    this.setState({
      [type]: value
    })
  }

  register = () => {
    const { username, password } = this.state
    if (!username || !password) {
      message.warning('用户名和密码必填项！')
      return
    }
    request.post('/system/hr/reg', {username, password}, {}, true).then(res => {

    })
  }

  render() {
    const { username, password } = this.state;
    return (
      <div className="register-container">
        <ul>
          <li>
            <Input placeholder="请输入用户名" value={username} onChange={this.changeInput('username')} />
          </li>
          <li>
            <Input placeholder="请输入密码" value={password} onChange={this.changeInput('password')} />
          </li>
          <Button type="primary" onClick={this.register}>注册</Button>
        </ul>
      </div>
    )
  }
}
export default Register;
